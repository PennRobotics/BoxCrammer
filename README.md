# BoxCrammer

## General idea

A rectangular box nested inside another can be in one of 48 space-maximizing combinations of position-plus-orientation. However, due to the symmetry of the box, I think it's actually only six: purely orientation changes, as every corner-placed box will touch the width wall, height wall, and depth wall.

However, the next box to be placed could be placed against any corner (inner vertex) of any valid combination from above (Case A) OR placed inside the smaller box (Case B).

The symmetry from earlier is broken by the new box (unless it isn't e.g. depth and height of either box are the same length) so the added box could go into one of the seven untouched corners&mdash;unless the first added box takes up the entire length of one or more dimensions&mdash; or one of the three new vertices where the new box intersects the old box e.g. along the height edge, width edge, or depth edge.

At this point, the next box could go in any of six orientations, creating up to 60 new possibilities in Case A or six in Case B.

We would need to identify all of the mirror copies for each type of box and eliminate these from all of the possible outcomes. Then, a new box could be introduced into any of the possible vertices. You could see how this dynamic programming solution quickly gets out of hand. You would ideally need some method of pruning, but it's also maybe tricky to prune. If you have a large cube and fill it with a bunch of plate-like boxes (same height and depth but very small width&hellip; say 35 by 35 by 5 cm wide into a 35 cm cube) and then have another assortment of slightly thicker plate-like boxes (in this example, five boxes, each 7 cm wide), there is no way to fit the new boxes without removing some. However, you can put 5 of the smallest boxes inside the slightly wider ones and then have two on the side.

You won't know what to prune unless you see all of the boxes unless you know for sure that the volume of all unseen boxes is larger than the available volume of any subspace inside.

Oh, I haven't even gotten to the best part. If the top is always open, you could jam a larger box inside of a smaller box as long as the smaller one has two dimensions that are larger than the larger box. In other words, one box shaped like a hotdog would fit inside another shaped like a hamburger as long as the hotdog-shaped box is allowed to stick out of the hamburger-shaped one.

### Possible heuristics

One idea is to convert all of the boxes to mass-having spheres and then simulate aggressively shaking the entire system and letting the small ones settle at the bottom. This doesn't really work because the spheres have to go inside on another.

Another is to start with a multiples system: Box Type I is a 5 cm cube. Box Type II is a 10 cm cube. Box Type III is a 20 cm cube. Then it is rather clear how to pack boxes. A new box with 15 cm sides would complicate things but still have a reasonable solution space.

Extend this to include rectangles (e.g. 5 x 5 x 10, 10 x 10 x 20, 5 x 10 x 20) and then orientation starts to matter but solutions should still be reasonably findable.

At this point, two things need to happen: A minimum wall thickness needs to be introduced, else all of the 5 cm cubes could nest inside one another. (In this case, make the larger boxes 10.1 and the next size 20.3 or something similar.) Boxes that are almost but not quite Type I could be "upconverted" to Type I, almost Type II &rarr; Type II, etc. This whole thing should work out like a fractal or Russian nesting doll. Then, you only need exceptions for the super long or super flat boxes.

In this way, you should be able to sort boxes by volume if they are reasonably shaped (not extremely different along a single dimension) and then drop those into the solution database&mdash;a lookup table of where all those boxes should go for the several hundred possibilities at that scale.

Then, the outermost box could be grouped with all of the other outermost boxes having the same volume. At this point, you'd have the same several hundred possibilities but a different scale. The lookup table would be the same.

The exception boxes would be the hard part: the tennis rackets and poster boards of the packing world. In this case, I think I would examine if there are internal cavities that _could_ be large enough to hold a weird-shaped box and then move all of the boxes at that scale randomly and check if the weird-shaped box _can_ fit when all the boxes have moved.

The alternative would be to subtract space from the largest box that can fit a strange object, and use that subtracted space to check for solutions.

That would mean creating a new group of solutions for boxes that are not cube-shaped. I would honestly go with something like the ISO 216 standard for paper, e.g. the A4, B4, A5, B5, A6 sizes. Here, the height of one size is the width of the next higher size, and the width and height are always a multiple of the square root of two. The difficult part would be combining the cube system and the rectangle system, but remember that rectangles can always be promoted to a slightly larger size that fits within the cube system.

Of course, that means getting perfect fits while mixing systems would be nearly impossible. But I think that's where volume could be used to check if a box should be checked anyway, and then shimmy the inner and outer around to different places at random to check if a valid solution can be found.

So then how do you find the balance between the different systems? When should weird boxes be prioritized? Checking if a violating rectangle shape still fits in a cube shape? You could just cycle through each of the implementations, but that would almost certainly be the naive but wrong solution. Perhaps better is a rating system of how perfect a fit is. If all of the boxes of a system meet the rules of that system with absolutely no gaps but all possible spaces filled, it is simply ranked a perfect fit (10/10) and shouldn't be replaced by a different heuristic.

Something like fitting a weird box into a larger, more standard box? That is maybe a 4/10 and can be reverted to try something else six out of ten new trials.

Then, probably a way to develop these heuristics is to generate test cases of nearly perfect fits using a variety of boxes. Manually step through the solution. Any time a heuristic provides a wrong solution, lower its fit rating.
