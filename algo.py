'''
* "Box" object: id, w, d, h, parent
  * Derived: vol, children
* Until further notice, this ONLY deals with boxes
* Sort all boxes by volume. Regardless of other dimensions, a box will never be able to fit inside a box with smaller volume. (This can also be used to prune out which subdivisions might work: Two boxes of volume 1200 cc will not fit inside a 2200 cc box.)
* Assume outer dimensions; inner dimension will be 1 cm less than outer (5mm walls)
* Create a "fits inside" list: `fits_list = [(i, j) for i, l_i in enumerate(box_list) for j, l_j in enumerate(box_list[i+1:], i+1) if <condition using l_j and l_i>]`
* There needs to be a way to prune this list whenever a box is tested for nesting others.
* If a small box is placed inside a large box, the available space for new boxes can be divided into several possibilities: tall spaces and wide spaces of various dimensions
  * Several small boxes would further restrict the available space while providing a more detailed restriction space for new boxes. It might be possible to have a very tall and narrow box (e.g. book-shaped) and a bunch of small cubes, but everything fits in one exact possible packing.
  * At some point, if a box is packed to some percentage of its volume or with only a small margin available for new boxes, it should be declared "filled" and removed from any calculation queue.
* If a box A is 30W x 25D x 20H and another box B of dimension 12W x 9D x 8H is inserted inside, now there are a handful of possibilities of remaining space in box A:
  * Without rotation:    17 x 15 x 11  (w=ow, d=od, h=oh)
  * Rotate forward:      17 x 16 x 10  (w=ow, d=oh, h=od)
  * Rotate right-up:     21 x 15 x  7  (w=oh, d=od, h=ow)
  * Rotate CW:           20 x 12 x 11  (w=od, d=ow, h=oh)
  * Forward + right-up:  20 x 16 x  7  (w=od, d=oh, h=ow)
  * Forward + CW:        21 x 12 x 10  (w=oh, d=ow, h=od)
  * NOTE: The remaining space calculation can allow any TWO dimensions to expand to the outer box's dimensions.
* In solving, you pretty much want to minimize either the surface area or volume of boxes with no parents.
    This will start with all the boxes that cannot fit into any others
'''

class BoxSolver:
    def __init__(self, box_list: list[Box]):
        self.box_list = box_list
        self.box_tree = []  # TODO
        self.solved = False

    def solve(self):
        for box in self.box_list:
            print(box)  # TODO
        self.solved = True

    def simple_print(self):
        for indent, box in self.box_tree:
            print(' '*indent + box.id)  # TODO


class Box:
    def __init__(self, w, d, h, b_name='', parent=None):
        self.id = b_name
        self.w = w
        self.d = d
        self.h = h
        self.parent = parent
        self.vol = w * d * h
        self.contains = []

    def get_vol(self) -> int:
        return self.vol

    def get_free_vol(self) -> int:
        return self.vol - sum([box.get_vol() for box in self.contains])

    def get_free_spaces(self) -> List[tuple]:
        # TODO: In the naive case, solve by populating all voxels and return largest containers of exclusively unfilled voxels
        #   There's probably a mathematical solution, too.
        #   In both cases, this is complicated because contained boxes could be repositioned internally but only in valid configurations.
        #   At some point, it probably makes sense to round to the space occupied by the larger of multiple boxes
        return [()]


def main():
    # TODO: import box list from CSV
    None

    # TODO: create boxes
    None
    b_test_0 = Box(20, 30, 16, 'A')
    b_test_1 = Box( 4, 22, 14, 'B')
    b_test_2 = Box(12, 26, 12, 'C')
    b_test_3 = Box(10,  8,  9, 'D')
    b_test_4 = Box(11,  9,  8, 'E')

    box_list = [b_test_2, b_test_4, b_test_1, b_test_0, b_test_3]

    # TODO: solve optimization problem
    bs = BoxSolver(box_list)
    bs.solve()

    # TODO: output
    bs.simple_print()
    pass


main()
